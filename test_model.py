#!/usr/bin/env python3
import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
import copy
import math
import os
import sys
import time
import pickle as pkl
import yaml
import json
from omegaconf.dictconfig import DictConfig
from omegaconf.listconfig import ListConfig



from video import VideoRecorder
from logger import Logger
from replay_buffer import ReplayBuffer
import utils

import dmc2gym
import hydra


def make_custom_env(cfg):
    env = hydra.utils.instantiate(cfg.env)
    env._max_episode_steps = cfg.env.params._max_episode_steps
    return env


class Workspace(object):
    def __init__(self, path, load='latest'):

        with open(f'{path}/config.yaml') as cfg_file:
            cfg = DictConfig(yaml.load(cfg_file,yaml.FullLoader))
        self.device = torch.device(cfg.device)

        if 'custom_env' in cfg:
            cfg.env = cfg.custom_env



        cfg.env.params.headless=False

        

        self.env = make_custom_env(cfg)

        self.agent = hydra.utils.instantiate(cfg.agent)

        if load == 'latest':
            load = max([int(x) for x in os.listdir(f'{path}/model')])

        if not os.path.exists(f'{path}/model/{load}'):
            print("Load step does not exist. Available steps are:")
            for x in os.listdir(f'{path}/model'):
                print(x)
            print("Will now run a vanilla experiment")
        else:
            self.agent.load_model(f'{path}/model/{load}')




    def run(self,sample = False, episodes=5, print_progress = False):
        for ep in range(int(episodes)):
            obs = self.env.reset()
            self.agent.reset()
            done = False
            episode_reward = 0
            step = 0
            env_info = None
            success = []
            while not done:
                with utils.eval_mode(self.agent):
                    action = self.agent.act(obs, sample=sample, env_info=env_info)
                obs, reward, done, env_info = self.env.step(action)
                episode_reward += env_info['overallReward']
                # success.append(env_info['Success'])
                step += 1
                if print_progress:
                    print(f"\nStep: {step}")
                    print(f"Action: {action}")
                    # print(f"Subtask: {env_info['subtask']}")

            print(f'Episode {ep} \n   Overall Reward : {episode_reward}')

        


def arg(tag):
    if tag in sys.argv:
        val = sys.argv[sys.argv.index(tag)+1]
    else:
        val = None
    return val

def getArgs(tags):
    arguments = {}
    for tag in tags:
        if arg(tag) is not None:
            arguments[tag[2:]] = arg(tag)
    return arguments

# @hydra.main(config_path=('{}/config.yaml'.format()), strict=True)
def main():
    workArgs = getArgs(['--path','--load'])
    workspace = Workspace(**workArgs)
    runArgs = getArgs(['--sample','--episodes','--print_progress'])
    workspace.run(**runArgs)


if __name__ == '__main__':
    main()