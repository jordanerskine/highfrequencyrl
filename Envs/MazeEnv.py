import numpy as np
import cv2,os, math

from gym import spaces
from copy import deepcopy as copy


class mazeEnv:
    def __init__(self,**kwargs):

        self.hf = False if "highFrequency" not in kwargs else kwargs["highFrequency"]
        self._max_episode_steps = 1000 if "_max_episode_steps" not in kwargs else kwargs["_max_episode_steps"]
        self.random_target = True if "random_target" not in kwargs else kwargs["random_target"]
        self.normalise = True if "normalise" not in kwargs else kwargs["normalise"]
        self.headless = False if "headless" not in kwargs else kwargs["headless"]
        self.mazeName = "Small" if "mazeName" not in kwargs else kwargs["mazeName"]
        self.curriculum = False if "curriculum" not in kwargs else kwargs["curriculum"]
        self.sparse = False if "sparse" not in kwargs else kwargs["sparse"]
        self.hfMag = 1 if "hfMag" not in kwargs else kwargs["hfMag"]
        self.observationSpace = 4
        if self.hf:
            self.matrixSize = 5
            self.hfMatrix = np.random.normal(size = [self.observationSpace,self.matrixSize,2])*self.hfMag
            self.observationSpace = 2*self.observationSpace*self.matrixSize
        
        self.actionSpace = 2
        self.ep = 0

        self.action_space = spaces.Box(low=-1, high=1, shape=[self.actionSpace], dtype=np.float64)
        self.observation_space = spaces.Box(low=-1, high=1, shape=[self.observationSpace])

        self.maze = cv2.imread(f"/media/jordan/Documents/Repos/HighFrequencyRL/Mazes/{self.mazeName}.PNG",0)
        self.ep_poses = []
        self.reset()

    def reset(self):
        self.epStep = 0
        self.ep += 1
        badColour = True
        self.target = (np.array(self.maze.shape)/2).astype(int)
        while badColour:
            self.pos = np.random.randint([0,0],self.maze.shape)
            if self.random_target:
                self.target = np.random.randint([0,0],self.maze.shape)
            if (self.maze[self.pos[0],self.pos[1]] > 100) and \
                (self.maze[self.target[0],self.target[1]] > 100):
                if (not self.curriculum or not self.headless) or np.linalg.norm(self.pos-self.target)<max(self.maze.shape)*self.ep/1000:
                    badColour = False


        # self.ep_poses = []
        if self.hf:
            obs = self._hfObs()
        else:
            obs = self._obs()
        return obs

    def reward(self):
        dist = np.linalg.norm(self.pos-self.target)
        if not self.sparse:
            return -dist
        else:
            return int(dist < 20)

    def _render(self):
        img = copy(self.maze)
        for i in range(len(self.ep_poses)-1):
            if self.ep_poses[i][1] == self.ep_poses[i+1][1]:
                img = cv2.line(img,self.ep_poses[i][0],self.ep_poses[i+1][0],color = (0,0,0),thickness = 1)

        cv2.drawMarker(img,tuple(reversed(self.pos)),color = (0,200,0),thickness=5)
        cv2.drawMarker(img,tuple(reversed(self.target)),color = (200,0,0),thickness=5)
        cv2.imshow("Maze",img)
        cv2.waitKey(1)

    def step(self,act):
        act = np.round(act*5).astype(int)
        nPos = self.pos+act
        # cv2.imshow("clip",self.maze[nPos[0]-100:nPos[0]+100,nPos[1]-100:nPos[1]+100]); cv2.waitKey(1)
        if np.mean(self.maze[nPos[0],nPos[1]]) > 100:
            self.pos += act
        rew = self.reward()
        # else:
        #     rew = self.reward()-1

        done = False
        if self.sparse and rew != 0:
            done = True

        if self.hf:
            obs = self._hfObs()
        else:
            obs = self._obs()

        self.epStep += 1
        if not self.headless:
            self.ep_poses.append([tuple(reversed(self.pos)),self.ep])
            self._render()
        done = (self.epStep > self._max_episode_steps) or done
        info = {"overallReward":rew}

        return obs,rew, done, info

    def _hfObs(self):
        obs = np.expand_dims(self._obs(),1)
        vals = self.hfMatrix[:,:,0]*(obs+self.hfMatrix[:,:,1])
        return np.ndarray.flatten(np.concatenate([np.sin(vals),np.cos(vals)]))

    def _obs(self):
        if self.normalise:
            return np.concatenate([np.divide(self.pos,self.maze.shape),np.divide(self.target,self.maze.shape)])
        else:
            return np.concatenate([self.pos,self.target])

    



if __name__ == "__main__":
    env = mazeEnv()
    env.step([-1,1])