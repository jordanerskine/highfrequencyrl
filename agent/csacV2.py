import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
import math
import time

from agent import Agent
import utils

import hydra


# ----- CSAV-V2 ------ #
# This algorithms uses a series of critic networks that each are trying to estimate
# the Q value corresponding to their specific subtask. A policy network is trained 
# to maximise a convex combination of these critic networks, the convex combination
# being defined by a latent vector that is passed into the policy network and used 
# in the loss. The critics are trained under the assumption that the policy network 
# is producing actions that maximise solely their critic function. Functionally, 
# this is done by setting the latent vector of the critic to maximise that specific
# critic. Ideally this algorithm will be able to pick between tasks based on the 
# latent vector. 



class CSAC(Agent):
    """SAC algorithm."""
    def __init__(self, obs_dim, action_dim, action_range, device, critic_cfg,
                 actor_cfg, discount, init_temperature, alpha_lr, alpha_betas,
                 actor_lr, actor_betas, actor_update_frequency, critic_lr,
                 critic_betas, critic_tau, critic_target_update_frequency,
                 batch_size, learnable_temperature,
                 nSubtasks):
        super().__init__()

        self.action_range = action_range
        self.device = torch.device(device)
        self.discount = discount
        self.critic_tau = critic_tau
        self.actor_update_frequency = actor_update_frequency
        self.critic_target_update_frequency = critic_target_update_frequency
        self.batch_size = batch_size
        self.learnable_temperature = learnable_temperature

        self.nSubtasks = nSubtasks

        self.critics = [hydra.utils.instantiate(critic_cfg).to(self.device) for _ in range(self.nSubtasks)]
        self.critic_targets = [hydra.utils.instantiate(critic_cfg).to(self.device) for _ in range(self.nSubtasks)]
        for critic, target in zip(self.critics,self.critic_targets):
            target.load_state_dict(critic.state_dict())

        actor_cfg.params.obs_dim = obs_dim + nSubtasks

        self.actor = hydra.utils.instantiate(actor_cfg).to(self.device)

        self.log_alpha = torch.tensor(np.log(init_temperature)).to(self.device)
        self.log_alpha.requires_grad = True
        # set target entropy to -|A|
        self.target_entropy = -action_dim

        # optimizers
        self.actor_optimizer = torch.optim.Adam(self.actor.parameters(),
                                                lr=actor_lr,
                                                betas=actor_betas)

        self.critic_optimizers = [torch.optim.Adam(self.critics[n].parameters(),
                                                 lr=critic_lr,
                                                 betas=critic_betas) for  n in range(self.nSubtasks)]

        self.log_alpha_optimizer = torch.optim.Adam([self.log_alpha],
                                                    lr=alpha_lr,
                                                    betas=alpha_betas)

        self.train()
        for critic_target in self.critic_targets:
            critic_target.train()

        self.times = dict(sample = [], actor_update = [], critic_update = [])

    def train(self, training=True):
        self.training = training
        self.actor.train(training)
        for critic in self.critics:
            critic.train(training)

    @property
    def alpha(self):
        return self.log_alpha.exp()

    def act(self, obs, sample=False, env_info = None):
        obs = torch.FloatTensor(obs).to(self.device)
        obs = obs.unsqueeze(0)
        lat = torch.FloatTensor(self.latent_vector).to(self.device).unsqueeze(0)
        inp = torch.cat([obs,lat],dim=1)
        dist = self.actor(inp)
        action = dist.sample() if sample else dist.mean
        action = action.clamp(*self.action_range)
        assert action.ndim == 2 and action.shape[0] == 1
        return utils.to_np(action[0])

    def reset(self):
        super().reset()
        if np.random.random() > 0.5:
            self.latent_vector = np.random.random([self.nSubtasks])
            self.latent_vector /= np.sum(self.latent_vector)
        else:
            ind = np.random.randint(0,self.nSubtasks)
            self.latent_vector = np.zeros([self.nSubtasks])
            self.latent_vector[ind] = 1

    def normalise(self, batch):
        mn = torch.min(batch).detach()
        mx = torch.max(batch).detach()
        return (batch-mn)/(mx-mn)

    def update_critics(self, obs, action, reward, next_obs, not_done, logger,
                      step):
        for n, critic in enumerate(self.critics):
            lat = torch.as_tensor([float(i==n) for i in range(self.nSubtasks)],device = self.device).unsqueeze(0).expand(self.batch_size,2)
            inp = torch.cat([next_obs,lat],dim=1)
            dist = self.actor(inp)
            next_action = dist.rsample()
            log_prob = dist.log_prob(next_action).sum(-1, keepdim=True)
            target_Q1, target_Q2 = self.critic_targets[n](next_obs, next_action)
            target_V = torch.min(target_Q1,
                                target_Q2) - self.alpha.detach() * log_prob
            target_Q = reward + (not_done * self.discount * target_V)
            target_Q = target_Q.detach()

            # get current Q estimates
            current_Q1, current_Q2 = critic(obs, action)
            critic_loss = F.mse_loss(current_Q1, target_Q) + F.mse_loss(
                current_Q2, target_Q)
            logger.log('train_critic/loss', critic_loss, step)

            # Optimize the critic
            self.critic_optimizers[n].zero_grad()
            critic_loss.backward()
            self.critic_optimizers[n].step()

            critic.log(logger, step)

    def update_actor_and_alpha(self, obs, latent_vector, logger, step):
        inp = torch.cat([obs,latent_vector],dim=1)
        dist = self.actor(inp)
        action = dist.rsample()
        log_prob = dist.log_prob(action).sum(-1, keepdim=True)
        actor_Qs = []
        for critic in self.critics:
            actor_Q1, actor_Q2 = critic(obs, action)

            actor_Qs.append(self.normalise(torch.min(actor_Q1, actor_Q2)))
        actor_Qs = torch.stack(actor_Qs,dim=1)
        full_q = torch.sum(torch.mul(latent_vector.unsqueeze(-1),actor_Qs),dim=1)
        actor_loss = (self.alpha.detach() * log_prob - (full_q)).mean()

        logger.log('train_actor/loss', actor_loss, step)
        logger.log('train_actor/target_entropy', self.target_entropy, step)
        logger.log('train_actor/entropy', -log_prob.mean(), step)

        # optimize the actor
        self.actor_optimizer.zero_grad()
        actor_loss.backward()
        self.actor_optimizer.step()

        self.actor.log(logger, step)

        if self.learnable_temperature:
            self.log_alpha_optimizer.zero_grad()
            alpha_loss = (self.alpha *
                          (-log_prob - self.target_entropy).detach()).mean()
            logger.log('train_alpha/loss', alpha_loss, step)
            logger.log('train_alpha/value', self.alpha, step)
            alpha_loss.backward()
            self.log_alpha_optimizer.step()

    def update(self, replay_buffer, logger, step):
        timer = time.time()
        obs, action, reward, next_obs, not_done, not_done_no_max, env_infos, latent_vector = replay_buffer.sample(
            self.batch_size, return_env_info = True, return_latent = True)
        if 'overallReward' in env_infos[0]:
            env_infos = {k: [dic[k] for dic in env_infos] for k in env_infos[0]}
            reward = torch.Tensor(env_infos['overallReward']).cuda().unsqueeze(1)
        self.times['sample'].append(time.time()-timer)
        timer = time.time()

        logger.log('train/batch_reward', reward.mean(), step)

        self.update_critics(obs, action, reward, next_obs, not_done_no_max,
                           logger, step)
        self.times['critic_update'].append(time.time()-timer)
        timer = time.time()

        if step % self.actor_update_frequency == 0:
            self.update_actor_and_alpha(obs, latent_vector, logger, step)
            self.times['actor_update'].append(time.time()-timer)
            timer = time.time()

        if step % self.critic_target_update_frequency == 0:
            for critic, target in zip(self.critics,self.critic_targets):
                utils.soft_update_params(critic, target,
                                     self.critic_tau)

    def reset_times(self):
        self.times = dict(sample = [], actor_update = [], critic_update = [])

    def print_times(self):
        print('---')
        for elem in self.times:
            print(f'{elem}: {np.mean(self.times[elem])}')

    def log_times(self, logger, step):
        for elem in self.times:
            logger.log(f'timing/{elem}', np.mean(self.times[elem]), step)

    def save_model(self, logger, step):
        logger.save_model('Actor',self.actor.return_state_dict(),step)
        for n, critic in enumerate(self.critics):
            critic_state = critic.return_state_dict()
            for q in critic_state:
                logger.save_model(f'Critic{n}/{q}',critic_state[q],step)

    def load_model(self, location):
        self.actor.load_model(torch.load(f'{location}/Actor.pt'))
        critic_state_dict = {}
        for n in range(self.nSubtasks):
            critic_state_dict['Q1'] = torch.load(f'{location}/Critic{n}/Q1.pt')
            critic_state_dict['Q2'] = torch.load(f'{location}/Critic{n}/Q2.pt')
            self.critics[n].load_model(critic_state_dict)


