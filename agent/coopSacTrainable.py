import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
import math
import time
import matplotlib.pyplot as plt
import matplotlib

from agent import Agent
from sac import SACAgent
import utils

import hydra


matplotlib.use('TKAgg')


class singleSAC(SACAgent):
    def __init__(self, *args, subtask_id, coop_ratio, rectify_norm):
        super().__init__(*args)
        self.subtask_id = subtask_id
        self.coop_ratio = coop_ratio
        self.obs_size = args[0]
        self.act_size = args[1]

        self.rectify_norm = rectify_norm

        self.nextAgent = None
        self.previousAgent = None

        self.times = dict(sample = [], actor_update = [], critic_update = [])


    def set_next_agent(self,agent):
        self.nextAgent = agent

    def set_previous_agent(self,agent):
        self.previousAgent = agent

    def normalise(self, batch):
        mn = torch.min(batch).detach()
        mx = torch.max(batch).detach()
        return (batch-mn)/(mx-mn)

    def batch_range(self, batch):
        return torch.max(batch).detach()-torch.min(batch).detach()

    def act(self, obs, cRatio, sample=False, env_info = None):
        cObs = torch.FloatTensor(self.cat_obs(obs, cRatio)).to(self.device)
        cObs = cObs.unsqueeze(0)
        dist = self.actor(cObs)
        action = dist.sample() if sample else dist.mean
        action = action.clamp(*self.action_range)
        assert action.ndim == 2 and action.shape[0] == 1
        self.plot_c_ratio_dist(obs,cRatio)
        return utils.to_np(action[0])

    def cat_obs(self,obs,cRatio):
        size = obs.shape[0] if len(obs.shape) > 1 else 1
        if isinstance(obs,torch.cuda.FloatTensor):
            return torch.cat([obs,torch.as_tensor([float(cRatio) for _ in range(size)],device=self.device).unsqueeze(1)],dim=1)
        else:
            return np.concatenate([obs,np.array([cRatio for _ in range(size)])])

    def plot_c_ratio_dist(self,obs,cRatio):
        obs = torch.FloatTensor(obs)
        cRatios = torch.as_tensor([n/20 for n in range(21)]).float()
        altObs = torch.stack([obs for _ in range(21)])
        cObs = torch.cat([altObs,cRatios.unsqueeze(1)],dim=1)
        out = self.actor(cObs.cuda())

        act = out.mean
        net1 = self.critic(altObs.cuda(),act)
        net1 = torch.min(net1[0],net1[1])
        net1 = self.normalise(net1)

        if self.nextAgent is not None:
            net2 = self.nextAgent.critic(altObs.cuda(),act)
            net2 = torch.min(net2[0],net2[1])
            net2 = self.normalise(net2)

            comb = cRatios.cuda()*net1+(1-cRatios.cuda())*net2

            net2 = net2.cpu().detach()
            comb = comb.cpu().detach()

        net1 = net1.cpu().detach()

            

        mean = out.mean.cpu().detach().numpy().squeeze(1)
        scale = out.scale.cpu().detach().numpy().squeeze(1)

        f, (ax1, ax2) = plt.subplots(2,1)
        plt.ion()
        plt.cla()
        ax1.axis([0,1,-1,1])
        ax2.axis([0,1,0,1])
        plt.show()
        ax1.plot(cRatios,mean,color='red')
        ax2.fill_between(cRatios,mean-scale,mean+scale,alpha=0.2,color='red')
        plt.grid(True)
        plt.title(f"Policy {self.subtask_id}")

        ax2.plot(cRatios,net1,color='red')
        if self.nextAgent is not None:
            ax2.plot(cRatios,net2,color='blue')
            ax2.plot(cRatios,comb,color='green')

        plt.axvline(x=cRatio)
        plt.xlabel("Cooperative Ratio")
        plt.ylabel("Action")
        
        plt.draw()
        plt.pause(0.001)


    def update_actor_and_alpha(self, obs, logger, step):
        # The n here represents the next domain. The actor needs to be 
        # trained with respect to the next domain and critic


        # Get the loss for the no future agent case
        if self.nextAgent is None:
            dist = self.actor(self.cat_obs(obs, 1))
            action = dist.rsample()
            log_prob = dist.log_prob(action).sum(-1, keepdim=True)
            actor_Q1, actor_Q2 = self.critic(obs, action)

            actor_Q = torch.min(actor_Q1, actor_Q2)
            actor_loss = (self.alpha.detach() * log_prob - actor_Q).mean()
            all_log_probs = log_prob

        # Get the loss for the case with a future agent
        else:
            sampledCRatios = 5

            sampledCRatios = int(sampledCRatios)
            start = time.time()

            altObs = torch.stack([obs for _ in range(sampledCRatios)])
            cRatios = torch.as_tensor(np.random.rand(sampledCRatios))
            altCRatios = torch.stack([cRatios for _ in range(self.batch_size)],dim=1).unsqueeze(2).cuda().float()
            cObs = torch.cat([altObs,altCRatios],dim=2).view(self.batch_size*sampledCRatios,self.obs_size+1)

            aObs = altObs.view(self.batch_size*sampledCRatios,self.obs_size)
            cRatios = altCRatios.view(self.batch_size*sampledCRatios,1)

            dist = self.actor(cObs)
            action = dist.rsample()
            log_prob = dist.log_prob(action).sum(-1, keepdim=True)
            n_actor_Q1, n_actor_Q2 = self.nextAgent.critic(aObs, action)
            actor_Q1, actor_Q2 = self.critic(aObs, action)

            n_actor_Q = torch.min(n_actor_Q1, n_actor_Q2)
            actor_Q = torch.min(actor_Q1, actor_Q2)

            norm_n_Q = self.normalise(n_actor_Q)
            norm_Q = self.normalise(actor_Q)

            if self.rectify_norm:
                scaling_factor = cRatios*self.batch_range(actor_Q)+(1-cRatios)*self.batch_range(n_actor_Q)
            else:
                scaling_factor = 1

            actor_loss = (self.alpha.detach()*log_prob - scaling_factor*(cRatios*norm_Q + (1-cRatios)*norm_n_Q)).mean()



        logger.log(f'train/actor_{self.subtask_id}/loss', actor_loss, step)
        logger.log(f'train/actor_{self.subtask_id}/target_entropy', self.target_entropy, step)
        logger.log(f'train/actor_{self.subtask_id}/entropy', -log_prob.mean(), step)

        # optimize the actor
        self.actor_optimizer.zero_grad()
        actor_loss.backward()
        self.actor_optimizer.step()

        self.actor.log(logger, step, suffix=f'_{self.subtask_id}')

        if self.learnable_temperature:
            self.log_alpha_optimizer.zero_grad()
            alpha_loss = (self.alpha *
                          (-log_prob - self.target_entropy).detach()).mean()
            logger.log(f'train/alpha_{self.subtask_id}/loss', alpha_loss, step)
            logger.log(f'train/alpha_{self.subtask_id}/value', self.alpha, step)
            alpha_loss.backward()
            self.log_alpha_optimizer.step()

    def update_critic(self, obs, action, reward, next_obs, not_done,  
                      logger, step,
                      p_obs = None, p_action = None, p_reward = None, p_next_obs = None, p_not_done = None):
        # The p refers to the previous domain. The critic needs to be trained
        # with respect to the current and the previous domain
        
        # Get loss for the current domain
        if obs is None:
            noCurrent = True
        else:
            noCurrent = False
            dist = self.actor(self.cat_obs(next_obs,1))
            next_action = dist.rsample()
            log_prob = dist.log_prob(next_action).sum(-1, keepdim=True)
            target_Q1, target_Q2 = self.critic_target(next_obs, next_action)
            target_V = torch.min(target_Q1,
                                target_Q2) - self.alpha.detach() * log_prob
            target_Q = reward + (not_done * self.discount * target_V)
            target_Q = target_Q.detach()

            # get current Q estimates
            current_Q1, current_Q2 = self.critic(obs, action)
            current_critic_loss = F.mse_loss(current_Q1, target_Q) + F.mse_loss(
                current_Q2, target_Q)

        
        # Get loss for the previous domain
        if self.previousAgent is not None:
            dist = self.previousAgent.actor(self.cat_obs(p_next_obs,0))
            next_action = dist.rsample()
            log_prob = dist.log_prob(next_action).sum(-1, keepdim=True)
            target_Q1, target_Q2 = self.critic_target(p_next_obs, next_action)
            target_V = torch.min(target_Q1,
                                target_Q2) - self.alpha.detach() * log_prob
            target_Q = reward + (p_not_done * self.discount * target_V)
            target_Q = target_Q.detach()

            # get current Q estimates
            current_Q1, current_Q2 = self.critic(p_obs, p_action)
            previous_critic_loss = F.mse_loss(current_Q1, target_Q) + F.mse_loss(
                current_Q2, target_Q)

            if noCurrent:
                critic_loss = previous_critic_loss
            else:
                critic_loss = current_critic_loss + previous_critic_loss
        else:
            if noCurrent:
                critic_loss = None
            else:
                critic_loss = current_critic_loss

        # if not noCurrent:
        #     logger.log(f'train_critic_{self.subtask_id}/loss_current_domain', current_critic_loss, step)
        # if self.previousAgent is not None:
        #     logger.log(f'train_critic_{self.subtask_id}/loss_previous_domain', previous_critic_loss, step)
        if critic_loss is not None:
            logger.log(f'train/critic_{self.subtask_id}/loss', critic_loss, step)


        # Optimize the critic
        if critic_loss is not None:
            self.critic_optimizer.zero_grad()
            critic_loss.backward()
            self.critic_optimizer.step()

            self.critic.log(logger, step, suffix=f"_{self.subtask_id}")
    
    def update(self, replay_buffer, logger, step):
        # Get data from this agent's domain
        timer = time.time()
        try:
            obs, action, reward, next_obs, not_done, not_done_no_max = replay_buffer.subtask_sample(
                self.batch_size, subtask_id = self.subtask_id)
        except:
            obs, action, reward, next_obs, not_done, not_done_no_max = None, None, None, None, None, None

        # Get data from the previous agent's domain
        if self.previousAgent is not None:
            p_obs, p_action, p_reward, p_next_obs, p_not_done, p_not_done_no_max, p_env_infos = replay_buffer.subtask_sample(
                self.batch_size, subtask_id = self.subtask_id-1, return_env_info=True)
            # p_reward = torch.as_tensor(np.array([info[f'reward{self.subtask_id}'] for info in p_env_infos]))

        self.times['sample'].append(time.time()-timer)
        timer = time.time()

        if reward is not None:
            logger.log(f'train/agent_{self.subtask_id}/batch_reward', reward.mean(), step)

        if obs is not None:
            if self.previousAgent is not None:
                self.update_critic(obs, action, reward, next_obs, not_done_no_max,
                            logger, step,
                            p_obs, p_action, p_reward, p_next_obs, p_not_done_no_max)    
            else:
                self.update_critic(obs, action, reward, next_obs, not_done_no_max,
                                logger, step)
            self.times['critic_update'].append(time.time()-timer)
            timer = time.time()

        if step % self.actor_update_frequency == 0 and obs is not None:
            self.update_actor_and_alpha(obs, logger, step)
            self.times['actor_update'].append(time.time()-timer)

        if step % self.critic_target_update_frequency == 0:
            utils.soft_update_params(self.critic, self.critic_target,
                                     self.critic_tau)


class CSAC(SACAgent):
    def __init__(self, obs_dim, action_dim, action_range, device, critic_cfg,
                 actor_cfg, discount, init_temperature, alpha_lr, alpha_betas,
                 actor_lr, actor_betas, actor_update_frequency, critic_lr,
                 critic_betas, critic_tau, critic_target_update_frequency,
                 batch_size, learnable_temperature,
                 nSubtasks, cooperative_ratio, rectify_norm):

        actor_cfg.params.obs_dim += 1
        

        args = [obs_dim, action_dim, action_range, device, critic_cfg,
                 actor_cfg, discount, init_temperature, alpha_lr, alpha_betas,
                 actor_lr, actor_betas, actor_update_frequency, critic_lr,
                 critic_betas, critic_tau, critic_target_update_frequency,
                 batch_size, learnable_temperature]
        # print(args)
        
        if not isinstance(cooperative_ratio,list):
            cooperative_ratio = [cooperative_ratio for _ in range(nSubtasks)]

        self.agents = [singleSAC(*args, subtask_id = i, coop_ratio=cooperative_ratio[i],rectify_norm=rectify_norm) for i in range(nSubtasks)]
        for n in range(1, len(self.agents)):
            self.agents[n-1].set_next_agent(self.agents[n])
            self.agents[n].set_previous_agent(self.agents[n-1])
        super().__init__(*args)

        

    
    def train(self, training = True):
        self.training = training
        for agent in self.agents:
            agent.train(training)

    def act(self, obs, sample = False, env_info = None):
        # obs = torch.FloatTensor(obs).to(self.device)
        # obs = obs.unsqueeze(0)
        subtask = env_info['subtask'] if env_info is not None else 0
        action = self.agents[subtask].act(obs, self.agents[subtask].coop_ratio, sample)
        return action

    def update(self, replay_buffer, logger, step):
        for agent in self.agents:
            agent.update(replay_buffer, logger, step)

    def reset_times(self):
        for agent in self.agents:
            agent.times = dict(sample = [], actor_update = [], critic_update = [])

    def print_times(self):
        print('---')
        for n, agent in enumerate(self.agents):
            print(f'Agent {n}')
            for elem in agent.times:
                print(f'   {elem}: {np.mean(agent.times[elem])}')

    def log_times(self, logger, step):
        for n, agent in enumerate(self.agents):
            for elem in agent.times:
                logger.log(f'timing/agent{n}/{elem}', np.mean(agent.times[elem]), step)
        



# if __name__ == "__main__":

    