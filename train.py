#!/usr/bin/env python3
import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
import copy
import math
import os
import sys
import time
import pickle as pkl
import yaml
import json
from omegaconf.dictconfig import DictConfig
from omegaconf.listconfig import ListConfig



from video import VideoRecorder
from logger import Logger
from replay_buffer import ReplayBuffer
import utils

import dmc2gym
import hydra
import git


def make_env(cfg):
    """Helper function to create dm_control environment"""
    if cfg.env == 'ball_in_cup_catch':
        domain_name = 'ball_in_cup'
        task_name = 'catch'
    else:
        domain_name = cfg.env.split('_')[0]
        task_name = '_'.join(cfg.env.split('_')[1:])

    env = dmc2gym.make(domain_name=domain_name,
                       task_name=task_name,
                       seed=cfg.seed,
                       visualize_reward=True)
    env.seed(cfg.seed)
    assert env.action_space.low.min() >= -1
    assert env.action_space.high.max() <= 1

    return env

def make_custom_env(cfg):
    env = hydra.utils.instantiate(cfg.env)
    env._max_episode_steps = cfg.env.params._max_episode_steps
    return env

def saveConfig(config, work_dir):
    newDict = unpeelDict(config)
    with open(f'{work_dir}/config.yaml','w') as outfile:
        yaml.dump(newDict,outfile)

def saveGitHash(work_dir):
    repo = git.Repo(work_dir,search_parent_directories=True)
    hash = repo.head.object.hexsha
    with open(f'{work_dir}/gitHash.txt','w') as outfile:
        outfile.write(hash)

def unpeelDict(dct):
    newDict = {}
    for elem in dct:
        if isinstance(dct[elem], DictConfig):
            newDict[elem] = unpeelDict(dct[elem])
        elif isinstance(dct[elem], ListConfig):
            newDict[elem] = unpeelList(dct[elem])
        else:
            newDict[elem] = dct[elem]
    return newDict

def unpeelList(lst):
    newList = []
    for elem in lst:
        newList.append(elem)
    return newList


def logEnvInfos(logger, envInfos, step):
    for tag in envInfos[0]:
        data = []
        for infoStep in envInfos:
            data.append(infoStep[tag])
        data = np.array(data)
        logger.log(f'env_info/{tag}_mean', np.mean(data),step)
        logger.log(f'env_info/{tag}_max', np.max(data),step)
        logger.log(f'env_info/{tag}_min', np.min(data),step)


class Workspace(object):
    def __init__(self, cfg):
        self.work_dir = os.getcwd()
        print(f'workspace: {self.work_dir}')

        self.cfg = cfg

        self.logger = Logger(self.work_dir,
                             save_tb=cfg.log_save_tb,
                             log_frequency=cfg.log_frequency,
                             agent=cfg.agent.name)

        utils.set_seed_everywhere(cfg.seed)
        self.device = torch.device(cfg.device)
        self.env = make_custom_env(cfg)
        try:
            obsShape = self.env.observation_space['observation'].shape
        except:
            obsShape = self.env.observation_space.shape
        cfg.agent.params.obs_dim = obsShape[0]
        cfg.agent.params.action_dim = self.env.action_space.shape[0]
        cfg.agent.params.action_range = [
            float(self.env.action_space.low.min()),
            float(self.env.action_space.high.max())
        ]
        if 'nSubtasks' in dir(self.env) and 'nSubtasks' in dir(cfg.agent.params):
            cfg.agent.params.nSubtasks = self.env.nSubtasks
            
        self.agent = hydra.utils.instantiate(cfg.agent)
        if 'load' in cfg:
            self.agent.load_model(cfg.load)

        subtasks = 1 if 'nSubtasks' not in cfg.agent.params else cfg.agent.params['nSubtasks']

        self.replay_buffer = ReplayBuffer(obsShape,
                                          self.env.action_space.shape,
                                          int(cfg.replay_buffer_capacity),
                                          self.device,
                                          subtasks)

        self.video_recorder = VideoRecorder(
            self.work_dir if cfg.save_video else None)
        self.step = 0
        saveConfig(cfg,self.work_dir)
        saveGitHash(self.work_dir)
        # with open(f'{self.work_dir}/config.json','w') as outfile:
        #     json.dump(dict(cfg),outfile)



    def evaluate(self):
        timer = time.time()
        average_episode_reward = 0
        overall_reward = 0
        for episode in range(self.cfg.num_eval_episodes):
            obs = self.env.reset()
            self.agent.reset()
            if 'latent_vector' in self.agent.__dict__:
                if self.agent.nSubtasks == 2:
                    self.agent.latent_vector = np.array([episode/(self.cfg.num_eval_episodes-1),
                                                        (1-episode/(self.cfg.num_eval_episodes-1))])
            self.video_recorder.init(enabled=(episode == 0))
            done = False
            episode_reward = 0
            env_info = None
            while not done:
                with utils.eval_mode(self.agent):
                    action = self.agent.act(obs, sample=False, env_info=env_info)
                obs, reward, done, env_info = self.env.step(action)
                if self.cfg.render:
                    self.video_recorder.record(self.env)
                episode_reward += env_info['overallReward']
                overall_reward += env_info['overallReward']

            if 'latent_vector' in self.agent.__dict__:
                self.logger.log(f'eval/overall_reward/latent{episode}', episode_reward,
                        self.step)
            average_episode_reward += episode_reward
            if self.cfg.render:
                self.video_recorder.save(f'{self.step}.mp4')
        self.logger.log('eval/duration', time.time()-timer,self.step)
        average_episode_reward /= self.cfg.num_eval_episodes
        self.logger.log('eval/episode_reward', average_episode_reward,
                        self.step)
        overall_reward /= self.cfg.num_eval_episodes
        self.logger.log('eval/overall_reward', overall_reward,
                        self.step)
        
        self.logger.dump(self.step)
        self.agent.save_model(self.logger, self.step)

    def run(self):
        last_eval = 0
        episode, episode_reward, done = 0, 0, True
        env_infos = []
        start_time = time.time()
        while self.step < self.cfg.num_train_steps:
            if done:
                if self.step > 0:
                    self.logger.log('train/duration',
                                    time.time() - start_time, self.step)
                    start_time = time.time()
                    if 'nSubtasks' in self.cfg.agent.params and not self.logger.dumped:
                        self.logger.populate_with_n_subtasks(self.cfg.agent.params.nSubtasks)
                    self.logger.dump(
                        self.step, save=(self.step > self.cfg.num_seed_steps))

                    self.logger.log('timing/episode',time.time()-ep_time,self.step)
                    self.logger.log('timing/action',action_times,self.step)
                    self.logger.log('timing/env_step',env_times,self.step)
                    self.logger.log('timing/train',update_times,self.step)

                    self.logger.log('train/length', episode_step,self.step)

                    self.agent.log_times(self.logger, self.step)
                    # self.agent.print_times()
                    self.agent.reset_times()
                    
             

                # evaluate agent periodically
                if self.step > 0 and self.step >= last_eval + self.cfg.eval_frequency:
                    last_eval = self.step
                    self.logger.log('eval/episode', episode, self.step)
                    self.evaluate()

                self.logger.log('train/episode_reward', episode_reward,
                                self.step)
                if len(env_infos) > 0 and len(env_infos[0]) > 0:
                    self.logger._get_env_info_mg(env_infos[0])
                    logEnvInfos(self.logger,env_infos,self.step)

                obs = self.env.reset()
                self.agent.reset()
                done = False
                episode_reward = 0
                episode_step = 0
                env_infos = []
                episode += 1


                action_times = 0
                update_times = 0
                env_times = 0
                ep_time = time.time()

                self.logger.log('train/episode', episode, self.step)

            # sample action for data collection
            if self.step < self.cfg.num_seed_steps:
                action = self.env.action_space.sample()
            else:
                with utils.eval_mode(self.agent):
                    timer = time.time()
                    action = self.agent.act(obs, sample=True, env_info=env_info)
                    action_times += (time.time() - timer)
                    

            # run training update
            if self.step >= self.cfg.num_seed_steps:
                timer = time.time()
                self.agent.update(self.replay_buffer, self.logger, self.step)
                update_times += (time.time() - timer)

            timer = time.time()
            next_obs, reward, done, env_info = self.env.step(action)
            env_times += (time.time() - timer)

            env_infos.append(env_info)


            # allow infinite bootstrap
            done = float(done)
            # done_no_max = 0 if episode_step + 1 == self.env._max_episode_steps else done
            done_no_max = 0
            episode_reward += reward

            latent_vector = None if "latent_vector" not in self.agent.__dict__ else self.agent.latent_vector

            self.replay_buffer.add(obs, action, reward, next_obs, done,
                                   done_no_max, env_info, latent_vector)

            obs = next_obs
            episode_step += 1
            self.step += 1



@hydra.main(config_path='config/train.yaml',strict=False)
def main(cfg):
    workspace = Workspace(cfg)
    workspace.run()


if __name__ == '__main__':
    main()
